import sys
articulos = {}


def anadir(nombre, precio):
    articulos[nombre] = precio


def mostrar():
    for nombre, precio in articulos.items():
        print(f"{nombre}: {precio}€")


def pedir_articulo():
    articulo = input("Introduzca el nombre del artículo que desea comprar: ")
    while articulo not in articulos:
        print(f"El artículo {articulo} no está en la lista de artículos disponibles.")
        articulo = input("Introduzca el nombre del artículo que desea comprar: ")
    return articulo


def pedir_cantidad():
    cantidad = input("Introduzca la cantidad del artículo que desea comprar: ")
    while not cantidad.isdigit():
        print("La cantidad introducida no es un número.")
        cantidad = input("Introduzca la cantidad del artículo que desea comprar: ")
    return float(cantidad)


def main():
    mostrar()

    articulo = pedir_articulo()
    cantidad = pedir_cantidad()

    print(f"Se ha comprado {cantidad} unidades del artículo {articulo}")


sys.argv.pop(0)

if __name__ == '__main__':
    main()
